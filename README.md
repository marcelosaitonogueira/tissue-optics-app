# Tissue optics app

Our computer app was developed for teaching basic tissue optics concepts such as tissue absorption, scattering, reflectance and fluence rates in infinite or semi-infinite media. No programming experience is necessary to use the app.